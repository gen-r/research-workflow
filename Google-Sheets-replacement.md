# Google Sheets – Online collaborative spreadsheets: Extending Ethercalc to work with 'R'

For this specific 'research workflow' the available Open Source 'web-based collaborative spreadsheets' do not meet our level of readiness to be recommended for use. This is not to say that Open Source tools like Ethercalc cannot be used for many other situations needing spreadsheet functionality.

Our specific workflow requirements are that such a spreadsheet tool can be programmatically accessed for use with 'R' statistical software. This means that an API is available and that a software package is available for 'R' to interact with the API.

The 'web-based collaborative spreadsheet' software that we identified as being the leader in its field and of a very high quality is called Ethercalc, which also has an API.

* Issues of reproducibility should be considered. These could be addresses with the use of Git in the data workflow portion. A solution to consider could be Git and Rstudio, with an addition of a way to interact with the spreadsheet. (Jon Tennant)

## Recommendation

Online collaborative spreadsheet: Extending Ethercalc to work with 'R'. Ethercalc should be evaluated as a candidate to extend it to being usable computationally with 'R'. Ethercalc: https://ethercalc.net/ 

The work would involve: 

1. An evaluation of Ethercalc's functionality to perform as a collaborative online spreadsheet with 'R' data. Ethercalc: https://ethercalc.net/ ; 
1. Assess how an 'R' package such as 'googlesheets' could be adapted or used as a blueprint to provide the same functionality for Ethercalc; 
1. If the evaluation showed that a workable system could be made, then the full work should be carried out. See: googlesheets: https://cran.r-project.org/web/packages/googlesheets/ and https://github.com/jennybc/googlesheets/

Notes on technical pointers for using Ethercalc API https://twitter.com/gittaca/status/1073091616657813504

