# research workflow

'Making a FOSS Research Workflow' based on a keynote presentation from the 13th Munin Conference (Tromsø, Norway) by Dr. Corina Logan, "We won't be... 'Bullied into Bad Science'", 28.11.2018, slides (slide 25) https://osf.io/sy9f7/

## Plans to replace Google Sheets

https://gitlab.com/gen-r/research-workflow/blob/master/Google-Sheets-replacement.md

## Before: free to use

![slide](slides/2018-11-28_Logan_Munin_Keynote_025_before.jpg)

## After: Open Source

![slide](slides/Logan_Munin_Keynote_281118_slide_25.jpg)

Image: 'Making a 'Pre-Publishing' Research Workflow Open Source' modification of slide 25 from the keynote presentation given at the 13th Munin Conference (Tromsø, Norway)  by Dr. Corina Logan, "We won't be... 'Bullied into Bad Science'", 28.11.2018, https://osf.io/sy9f7/ | See section 'Failing to make the complete workflow Open Source' for annotations https://genr.eu/wp/making-research-workflow-open-source